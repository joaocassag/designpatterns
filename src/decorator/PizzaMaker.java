
package decorator;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class PizzaMaker {
    
    public static void main(String[] args){
        Pizza base = new TomatoSauce(new Mozzarella(new PlainPizza()));
        
        System.out.println("Ingredients: "+ base.getDescription());
        
        System.out.println("Cost: "+ base.getCost());
    }
    
}
