
package decorator;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface Pizza {
    
    public String getDescription();
    
    public double getCost();
    
}
