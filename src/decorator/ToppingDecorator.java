
package decorator;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public abstract class ToppingDecorator implements Pizza {
    
    protected Pizza tempPizza;
    
    public ToppingDecorator(Pizza p){
        tempPizza = p;
    }
    
    public String getDescription(){
        return tempPizza.getDescription();
    }
    
    public double getCost(){
        return tempPizza.getCost();
    }
    
}
