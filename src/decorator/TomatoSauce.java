
package decorator;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class TomatoSauce extends ToppingDecorator {
    
    public TomatoSauce(Pizza p) {
        super(p);
        
        System.out.println("Adding Tomato Sauce");
    }
    
    public String getDescription(){
        return tempPizza.getDescription() + ", Tomato Sause";
    }
    
    public double getCost(){
        return tempPizza.getCost() + .35;
    }
}
