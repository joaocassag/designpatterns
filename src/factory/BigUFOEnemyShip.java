
package factory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class BigUFOEnemyShip extends EnemyShip {
    
    public BigUFOEnemyShip(){
        setName("Big UFO Enemy Ship");
        setAmtDamage(40.0);
    }
    
}
