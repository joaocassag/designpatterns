
package factory;

import java.util.Scanner;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class EnemyShipTesting {
    
    public static void main(String[] args){
        EnemyShipFactory factory = new EnemyShipFactory();
        EnemyShip ship = null;
        
        Scanner input = new Scanner(System.in);
        String option = "";
        System.out.println("Choose type of ship: (u/r/b)");
        
        
        if(input.hasNextLine()){
            option = input.nextLine();
            ship = factory.makeEnemyShip(option);
            
        }
        
        if(ship != null){
            doStuffEnemy(ship);
        } else{
            System.out.println("Enter u,r, or b");;
        }
        
        
        
    }

    private static void doStuffEnemy(EnemyShip ship) {
        ship.displayEnemyShip();
        ship.followHeroShip();
        ship.enemyShipShoots();
    }
    
}
