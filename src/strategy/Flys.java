
package strategy;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface Flys {
    
    String fly();
    
}

class ItFlys implements Flys {
    
    public String fly(){
        return "Flying";
    }
    
}

class CantFly implements Flys {
    
    public String fly(){
        return "Can't fly";
    }
    
}
