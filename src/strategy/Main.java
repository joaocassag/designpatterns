
package strategy;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class Main {
    
    public static void main(String[] args){
        Animal sparky = new Dog();
        Animal tweety = new Bird();
        
        System.out.println("Dog: "+sparky.tryToFly());
        System.out.println("Bird: "+tweety.tryToFly());
        
        sparky.setFlyingAblity(new ItFlys());
        System.out.println("Dog: "+sparky.tryToFly());
    }
    
}
