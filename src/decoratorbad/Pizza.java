
package decoratorbad;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public abstract class Pizza {
    
    public abstract void setDescription(String d);
    
    public abstract String getDescription();
    
    public abstract double getCost();
    
    public abstract boolean hasFontina();
    
}
