
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface ESWeapon {
    
    public String toString();
    
}
