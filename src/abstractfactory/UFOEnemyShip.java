
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class UFOEnemyShip extends EnemyShip {
    
    EnemyShipFactory factory;
    
    public UFOEnemyShip(EnemyShipFactory factory){
        this.factory = factory;
    }

    @Override
    void makeShip() {
        System.out.println("Making enemy ship "+getName());
        
        weapon = factory.addESGun();
        engine = factory.addESEngine();
    }
}
