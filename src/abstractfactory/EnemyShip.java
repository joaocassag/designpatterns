
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public abstract class EnemyShip {
    
    private String name;
    ESWeapon weapon;
    ESEngine engine;
    //private double speed;
    //private double amtDamage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    abstract void makeShip();
    
    public String toString(){
        String info = "The "+name+" has a top speed of "+engine+ " and attack power of "+ weapon;
        return info;
    }
    
    /*
    public void followHeroShip(){
        System.out.println(getName()+" is following the hero");
    }
    
    public void displayEnemyShip(){
        System.out.println(getName()+" is on the screen");
    }
    
    public void enemyShipShoots(){
        System.out.println(getName()+" attacks and does "+getAmtDamage());
    }
    */
    
}
