
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public abstract class EnemyShipBuilding {
    
    protected abstract EnemyShip makeEnemyShip(String type);
    
    public EnemyShip orderTheShip(String type){
        EnemyShip ship = makeEnemyShip(type);
        ship.makeShip();
        
        return ship;
    }
    
}
