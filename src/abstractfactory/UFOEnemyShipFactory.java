
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class UFOEnemyShipFactory implements EnemyShipFactory {

    @Override
    public ESWeapon addESGun() {
        return new ESUFOGun();
    }

    @Override
    public ESEngine addESEngine() {
        return new ESUFOEngine();
    }
    
}
