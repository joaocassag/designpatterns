
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface EnemyShipFactory {
    
    public ESWeapon addESGun();
    public ESEngine addESEngine();
    
    /*
    public EnemyShip makeEnemyShip(String type){
        
        EnemyShip ship = null;
        
        if(type.equalsIgnoreCase("u")){
            ship = new UFOEnemyShip();
        } else if(type.equalsIgnoreCase("r")){
            ship = new RocketEnemyShip();
        } else if(type.equalsIgnoreCase("b")){
            ship = new BigUFOEnemyShip();
        } else {
            //will return null 
        }
        return ship;
    }
    */
    
}
