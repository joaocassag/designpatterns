
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class UFOEnemyShipBuilding extends EnemyShipBuilding {

    @Override
    protected EnemyShip makeEnemyShip(String type) {
        EnemyShip ship = null;
        
        if(type.equalsIgnoreCase("u")){
            EnemyShipFactory factory = new UFOEnemyShipFactory();
            ship = new UFOEnemyShip(factory);
            ship.setName("UFO Grunt Ship");
        } else if(type.equalsIgnoreCase("b")){
            EnemyShipFactory factory = new UFOBossEnemyShipFactory();
            ship = new UFOBossEnemyShip(factory);
            ship.setName("UFO Boss Ship");
        } 
        return ship;
    }
    
}
