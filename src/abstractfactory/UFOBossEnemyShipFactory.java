
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class UFOBossEnemyShipFactory implements EnemyShipFactory {

    @Override
    public ESWeapon addESGun() {
        return new ESUFOGun();
    }

    @Override
    public ESEngine addESEngine() {
        return new ESUFOEngine();
    }
}
