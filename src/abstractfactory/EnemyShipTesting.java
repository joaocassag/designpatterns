
package abstractfactory;

import java.util.Scanner;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class EnemyShipTesting {
    
    public static void main(String[] args){
        EnemyShipBuilding makeUFOs = new UFOEnemyShipBuilding();
        
        EnemyShip theGrunt = makeUFOs.orderTheShip("u");
        System.out.println(theGrunt+"\n");
        
        EnemyShip theBoss = makeUFOs.orderTheShip("b");
        System.out.println(theBoss+"\n");
    }
}
