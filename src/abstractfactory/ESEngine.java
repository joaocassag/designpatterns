
package abstractfactory;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface ESEngine {
    
    public String toString();
    
}
