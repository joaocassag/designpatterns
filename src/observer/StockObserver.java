
package observer;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class StockObserver implements Observer {
    
    //ArrayList<Observer> observers;
    private double ibmPrice;
    private double applPrice;
    private double googPrice;
    
    private static int observerIDTracker = 0;
    private int observerID;
    private Subject stockGrabber;
    
    public StockObserver(Subject stockGrabber){
        this.stockGrabber = stockGrabber;
        this.observerID = ++observerIDTracker;
        
        System.out.println("New Observer: "+this.observerID );
        
        stockGrabber.register(this);
    }

    @Override
    public void update(double ibmPrice, double aaplPrice, double googPrice) {
        this.ibmPrice = ibmPrice;
        this.applPrice = aaplPrice;
        this.googPrice = googPrice;
        
        printThePrices();
    }

    private void printThePrices() {
        System.out.println(observerID+"\nIBM: "+ibmPrice+
            "\nAAPL: "+applPrice+ "\nGOOG: "+googPrice);
    }
    
}
