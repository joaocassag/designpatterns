
package observer;

import java.util.ArrayList;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class StockGrabber implements Subject {
    
    ArrayList<Observer> observers;
    private double ibmPrice;
    private double applPrice;
    private double googPrice;
    
    public StockGrabber(){
        observers = new ArrayList<Observer>();
    }
    

    @Override
    public void register(Observer o) {
        observers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        int observerIndex = observers.indexOf(o);
        
        System.out.println("Observer: "+(observerIndex+1)+ " deleted" );
        observers.remove(observerIndex);
    }

    @Override
    public void notifyObserver() {
        for(Observer observer: observers){
            observer.update(ibmPrice, applPrice, googPrice);
        }
    }
    
    public void setIBMPrice(double price){
        this.ibmPrice = price;
        notifyObserver();
    }
    
    public void setAAPLPrice(double price){
        this.applPrice = price;
        notifyObserver();
    }
    
    public void setGOOGPrice(double price){
        this.googPrice = price;
        notifyObserver();
    }
    
}
