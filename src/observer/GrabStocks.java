
package observer;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class GrabStocks {
    
    public static void main(String[] args){
        StockGrabber sg = new StockGrabber();
        StockObserver so = new StockObserver(sg);
        
        sg.setIBMPrice(197.00);
        sg.setAAPLPrice(677.00);
        sg.setGOOGPrice(676.00);
        
        StockObserver so2 = new StockObserver(sg);
        
        sg.setIBMPrice(197.00);
        sg.setAAPLPrice(677.00);
        sg.setGOOGPrice(676.00);
        
        sg.unregister(so);
        
        sg.setIBMPrice(197.00);
        sg.setAAPLPrice(677.00);
        sg.setGOOGPrice(676.00);
    }
    
}
