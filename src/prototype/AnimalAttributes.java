
package prototype;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface AnimalAttributes extends Cloneable {
    
    public void setName(String name);
    public String getName();
    
}
