
package prototype;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class Sheep extends Animal {
    
    public Sheep(){
        System.out.println("Sheep is made");
    }

    @Override
    public Animal makeCopy() {
        
        System.out.println("Sheep is being made");
        
        Sheep so = null;
        
        try {
            so = (Sheep) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Sheep.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return so;
    }
    
    public String toString(){
        return "Dolly is here";
    }
    
}
