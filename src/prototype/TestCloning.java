/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class TestCloning {
    
    public static void main(String[] args){
        CloneFactory cf = new CloneFactory();
        
        Animal sally = new Sheep();
        
        Sheep cloned = (Sheep) cf.getClone(sally);
        
        
        System.out.println(sally);
        System.out.println(cloned);
        
        System.out.println("Sally hashcode: "+System.identityHashCode(System.identityHashCode(sally)));
        System.out.println("Cloned hashcode: "+System.identityHashCode(System.identityHashCode(sally)));
    }
    
}
