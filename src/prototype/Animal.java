
package prototype;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public abstract class Animal implements AnimalAttributes {
    
    private String name;
    
    public abstract Animal makeCopy();
    
    @Override
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
}
