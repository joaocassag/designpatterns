
package prototype;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class CloneFactory {
    
    public Animal getClone(Animal a){
        return a.makeCopy();
    }
    
}
