
package builder;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class TestRobotBuilder {
    
    public static void main(String[] args){
        RobotBuilder old = new OldRobotBuilder();
    
        RobotEngineer engineer = new RobotEngineer(old);
        
        engineer.makeRobot();
        
        Robot first = engineer.getRobot();
        
        System.out.println("Robot Head" + first.getRobotHead());
        System.out.println("Robot Torso" + first.getRobotTorso());
        System.out.println("Robot Arms" + first.getRobotArms());
        System.out.println("Robot Legs" + first.getRobotLegs());
    }
    
    
    
}
