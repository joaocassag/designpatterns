
package builder;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public interface RobotBuilder {
    
    public void buildRobotHead();
    public void buildRobotTorso();
    public void buildRobotArms();
    public void buildRobotLegs();

    public Robot getRobot();
    
}
