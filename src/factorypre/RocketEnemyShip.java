
package factorypre;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class RocketEnemyShip extends EnemyShip {
    
    public RocketEnemyShip(){
        setName("Rocket Enemy Ship");
        setAmtDamage(10.0);
    }
    
}
