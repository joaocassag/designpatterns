
package factorypre;

import java.util.Scanner;

/**
 *
 * @author <joaocassa@gmail.com>
 */
public class EnemyShipTesting {
    
    public static void main(String[] args){
        EnemyShip ufoShip = null;
        
        Scanner input = new Scanner(System.in);
        String option = "";
        System.out.println("Choose type of ship: (U/R)");
        
        if(input.hasNextLine()){
            option = input.nextLine();
        }
        
        if(option.equalsIgnoreCase("u")){
            ufoShip = new UFOEnemyShip();
        } else if(option.equalsIgnoreCase("r")){
            ufoShip = new RocketEnemyShip();
        }
        
        
        doStuffEnemy(ufoShip);
    }

    private static void doStuffEnemy(EnemyShip ship) {
        ship.displayEnemyShip();
        ship.followHeroShip();
        ship.enemyShipShoots();
    }
    
}
